import React from "react";

const NewDetail = (props) => {
  const {
    title,
    urlToImage,
    content,
    author,
    publishedAt,
    url,
    description,
  } = props;

  return (
    <>
      <div>
        <p className="title">{title}</p>
        <div>
          <img className="images" src={urlToImage} alt={title} />
          <p className="source">{description}</p>
          <hr className="content-separator"></hr>
          <p>
            <a href={url}>{content}</a>
          </p>
          <p className="author">by {author}</p>
          <p className="published">{publishedAt}</p>
          <br></br>
          <hr className="article-separator"></hr>
        </div>
      </div>
    </>
  );
};

export default NewDetail;
