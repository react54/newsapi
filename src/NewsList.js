import React, { useState, useEffect } from "react";
import NewDetail from "../src/NewDetail";

import { Form } from "react-bootstrap";

const News = () => {
  const [newsapi, setNewsapi] = useState([]);
  const [search, setSearch] = useState("");
  const [filteredNews, setFilteredNews] = useState([]);

  useEffect(() => {
    getNews();
  }, []);

  useEffect(() => {
    setFilteredNews(
      newsapi.filter((anynew) => {
        return anynew.source.name.toUpperCase().includes(search.toUpperCase());
      })
    );
  }, [search, newsapi]);

  const getNews = async () => {
    var url =
      "http://newsapi.org/v2/top-headlines?" +
      "country=us&" +
      "apiKey=803fdd9b8517490d89d8c85ade466b8d";

    const data = await fetch(url);
    const news = await data.json();
    const dataArticle = news.articles;
    console.log(dataArticle);
    setNewsapi(dataArticle);
  };

  return (
    <div>
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Search News</Form.Label>
          <Form.Control
            type="email"
            placeholder="Search by source. eg CNN"
            onChange={(e) => setSearch(e.target.value)}
          />
        </Form.Group>
      </Form>
      <h1>News</h1>
      <br></br>
      {filteredNews.map((n, idx) => (
        <NewDetail key={idx} {...n} />
      ))}
    </div>
  );
};

export default News;
