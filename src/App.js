import React, { Component } from "react";
import NewsList from "./NewsList";

export default class App extends Component {
  render() {
    return (
      <div className="container-sm">
        <NewsList></NewsList>
      </div>
    );
  }
}
